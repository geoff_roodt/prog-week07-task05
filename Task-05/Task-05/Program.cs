﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_05
{
    class Program
    {
        static void Main(string[] args)
        {
            var greeting = Greeting();
            Console.WriteLine(greeting);
        }

        static string Greeting()
        {
            Console.WriteLine("Enter a greeting!");
            return Console.ReadLine();
        }
    }
}
